package shop.dev.choibk.api.board.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import shop.dev.choibk.api.board.model.BoardItem;
import shop.dev.choibk.api.board.model.BoardRequest;
import shop.dev.choibk.api.board.model.BoardUpdateRequest;
import shop.dev.choibk.api.board.service.BoardService;
import shop.dev.choibk.common.response.model.CommonResult;
import shop.dev.choibk.common.response.model.ListResult;
import shop.dev.choibk.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "게시글 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board")
public class BoardController {
    private final BoardService boardService;

    @ApiOperation(value = "게시글 등록")
    @PostMapping("/add")
    public CommonResult setBoard(@RequestBody @Valid BoardRequest request) {
        boardService.setBoard(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "게시글 조회")
    @GetMapping("/all")
    public ListResult<BoardItem> getBoards() {
        return ResponseService.getListResult(boardService.getBoards(),true);
    }
    
    @ApiOperation(value = "게시글 수정")
    @PutMapping("/rewrite/{id}")
    public CommonResult putBoard(@PathVariable long id, @RequestBody @Valid BoardUpdateRequest request) {
        boardService.putBoard(id, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "게시글 삭제")
    @DeleteMapping("/delete/{id}")
    public CommonResult delBoard(@PathVariable long id) {
        boardService.delBoard(id);

        return ResponseService.getSuccessResult();
    }
}

package shop.dev.choibk.api.board.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.dev.choibk.api.board.entity.Board;
import shop.dev.choibk.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardResponse {
    private Long id;
    private String title;
    private String contents;

    private BoardResponse(BoardResponseBuilder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.contents = builder.contents;
    }

    public static class BoardResponseBuilder implements CommonModelBuilder<BoardResponse> {
        private final Long id;
        private final String title;
        private final String contents;

        public BoardResponseBuilder(Board board) {
            this.id = board.getId();
            this.title = build().title;
            this.contents = build().contents;
        }

        @Override
        public BoardResponse build() {
            return new BoardResponse(this);
        }
    }
}

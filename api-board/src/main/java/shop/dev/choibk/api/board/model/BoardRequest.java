package shop.dev.choibk.api.board.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class BoardRequest {

    @ApiModelProperty(value = "글 제목", required = true)
    @NotNull
    private String title;

    @ApiModelProperty(value = "글 내용", required = true)
    private String contents;

    @ApiModelProperty(value = "작성 날짜(yyyy-mm-dd)", required = true)
    @NotNull
    private LocalDate DateCreate;

    @ApiModelProperty(value = "조회수", required = true)
    private Double viewCount;
}

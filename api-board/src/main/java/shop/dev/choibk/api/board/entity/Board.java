package shop.dev.choibk.api.board.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.dev.choibk.api.board.model.BoardRequest;
import shop.dev.choibk.api.board.model.BoardUpdateRequest;
import shop.dev.choibk.common.interfaces.CommonModelBuilder;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Board {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 50)
    private String title; // 글 제목

    @Column(nullable = false, length = 250)
    private String contents; // 글 내용

    @Column(nullable = false, length = 50)
    private LocalDate DateCreate; // 작성 날짜

    @Column(nullable = false, length = 50)
    private Double viewCount; // 조회수

    private Board(BoardBuilder builder) {
        this.title = builder.title;
        this.contents = builder.contents;
        this.DateCreate = builder.DateCreate;
        this.viewCount = builder.viewCount;
    }

    public void putBoard(BoardUpdateRequest request) {
        this.contents = request.getContents();
    }

    public static class BoardBuilder implements CommonModelBuilder<Board> {
        private final String title;
        private final String contents;
        private final LocalDate DateCreate;
        private final Double viewCount;

        public BoardBuilder(BoardRequest request) {
            this.title = request.getTitle();
            this.contents = request.getContents();
            this.DateCreate = request.getDateCreate();
            this.viewCount = request.getViewCount();
        }
        @Override
        public Board build() {
            return new Board(this);
        }
    }
}
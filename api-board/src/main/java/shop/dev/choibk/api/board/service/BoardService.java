package shop.dev.choibk.api.board.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.dev.choibk.api.board.entity.Board;
import shop.dev.choibk.api.board.model.BoardItem;
import shop.dev.choibk.api.board.model.BoardRequest;
import shop.dev.choibk.api.board.model.BoardUpdateRequest;
import shop.dev.choibk.api.board.repository.BoardRepository;
import shop.dev.choibk.common.response.model.ListResult;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;

    public void setBoard(BoardRequest request) {
        Board addData = new Board.BoardBuilder(request).build();

        boardRepository.save(addData);
    }

    public ListResult<BoardItem> getBoards() {
        List<Board> originList = boardRepository.findAll();
        List<BoardItem> result = new ArrayList<>();
        originList.forEach(item -> result.add(new BoardItem.BoardItemBuilder(item).build()));
        return ListConvertService.settingResult(result);
    }

    public void putBoard(long id, BoardUpdateRequest request) {
        Board originData = boardRepository.findById(id).orElseThrow(ClassCastException::new);
        originData.putBoard(request);

        boardRepository.save(originData);
    }

    public void delBoard(long id) {
        boardRepository.deleteById(id);
    }
}
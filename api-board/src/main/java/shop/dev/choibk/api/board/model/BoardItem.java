package shop.dev.choibk.api.board.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.dev.choibk.api.board.entity.Board;
import shop.dev.choibk.common.interfaces.CommonModelBuilder;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardItem {
    private Long id;
    private String title;
    private String contents;
    private LocalDate dateCreate;
    private Double viewCount;

    private BoardItem(BoardItemBuilder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.contents = builder.contents;
        this.dateCreate = builder.dateCreate;
        this.viewCount = builder.viewCount;
    }

    public static class BoardItemBuilder implements CommonModelBuilder<BoardItem> {
        private final Long id;
        private final String title;
        private final String contents;
        private final LocalDate dateCreate;
        private final Double viewCount;

        public BoardItemBuilder(Board board) {
            this.id = board.getId();
            this.title = board.getTitle();
            this.contents = board.getContents();
            this.dateCreate = board.getDateCreate();
            this.viewCount = board.getViewCount();
        }

        @Override
        public BoardItem build() {
            return new BoardItem(this);
        }
    }
}

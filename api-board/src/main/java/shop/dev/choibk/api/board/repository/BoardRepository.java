package shop.dev.choibk.api.board.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.dev.choibk.api.board.entity.Board;

public interface BoardRepository extends JpaRepository<Board, Long> {
}

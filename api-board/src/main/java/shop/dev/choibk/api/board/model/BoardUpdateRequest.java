package shop.dev.choibk.api.board.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BoardUpdateRequest {

    @NotNull
    @Length(min = 5, max = 250)
    private String contents;
}

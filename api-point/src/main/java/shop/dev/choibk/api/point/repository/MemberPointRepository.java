package shop.dev.choibk.api.point.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.dev.choibk.api.point.entity.MemberPoint;

public interface MemberPointRepository extends JpaRepository<MemberPoint, Long> {
}

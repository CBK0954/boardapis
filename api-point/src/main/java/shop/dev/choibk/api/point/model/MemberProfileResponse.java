package shop.dev.choibk.api.point.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberProfileResponse {
    @ApiModelProperty(notes = "회원 시퀀스")
    private Long memberId;

    @ApiModelProperty(notes = "회원 아이디")
    private String username;

    @ApiModelProperty(notes = "회원 이름")
    private String name;

    @ApiModelProperty(notes = "회원 활성화 여부")
    private Boolean isEnabled;
}
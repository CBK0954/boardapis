package shop.dev.choibk.api.point.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.dev.choibk.api.point.entity.MemberPoint;
import shop.dev.choibk.api.point.model.MemberProfileResponse;
import shop.dev.choibk.api.point.model.MyPointResponse;
import shop.dev.choibk.api.point.repository.MemberPointRepository;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PointService {
    private final MemberPointRepository memberPointRepository;

    public MyPointResponse getMyPoint(MemberProfileResponse memberProfileResponse) {
        Optional<MemberPoint> memberPoint = memberPointRepository.findById(memberProfileResponse.getMemberId());
        long currentPoint = 0L;
        if (memberPoint.isPresent()) currentPoint = memberPoint.get().getPointValue();

        return new MyPointResponse.Builder(memberProfileResponse.getMemberId(), memberProfileResponse.getName(), currentPoint).build();
    }
}
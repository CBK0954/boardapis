package shop.dev.choibk.api.member.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shop.dev.choibk.api.member.entity.Member;
import shop.dev.choibk.api.member.service.ProfileService;
import shop.dev.choibk.common.response.model.CommonResult;
import shop.dev.choibk.common.response.service.ResponseService;

@Api(tags = "권한테스트")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/test")
public class TestController {
    private final ProfileService profileService;
    @ApiOperation(value = "나의 회원정보 조회 - 아이디만 뿌리기")
    @GetMapping("/info")
    public String getMemberData() {
        Member member = profileService.getMemberData();
        return member.getUsername();
    }

    @ApiOperation(value = "관리자만 접근 가능")
    @GetMapping("/admin")
    public CommonResult testAdmin() {
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "유저만 접근 가능")
    @GetMapping("/user")
    public CommonResult testUser() {
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "로그인 사용자 전체 접근 가능")
    @GetMapping("/all")
    public CommonResult testAll() {
        return ResponseService.getSuccessResult();
    }
}
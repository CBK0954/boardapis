package shop.dev.choibk.api.member.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import shop.dev.choibk.api.member.entity.Member;
import shop.dev.choibk.api.member.model.MemberProfileResponse;
import shop.dev.choibk.api.member.model.PasswordChangeRequest;
import shop.dev.choibk.api.member.service.MemberDataService;
import shop.dev.choibk.api.member.service.ProfileService;
import shop.dev.choibk.common.response.model.CommonResult;
import shop.dev.choibk.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "회원 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberDataController {
    private final ProfileService profileService;
    private final MemberDataService memberDataService;


    @ApiOperation(value = "내 프로필 정보")
    @GetMapping("/profile")
    public MemberProfileResponse getProfile() {
        return profileService.getProfile();
    }

    @ApiOperation(value = "내 비밀번호 변경")
    @PutMapping("/my/password")
    public CommonResult putPasswordMy(@RequestBody @Valid PasswordChangeRequest changeRequest) {
        Member member = profileService.getMemberData();
        memberDataService.putPassword(member.getId(), changeRequest, false);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 비밀번호 변경 (관리자용)")
    @PutMapping("/password/member-id/{memberId}")
    public CommonResult putPasswordAdmin(@PathVariable long memberId, @RequestBody @Valid PasswordChangeRequest changeRequest) {
        memberDataService.putPassword(memberId, changeRequest, true);

        return ResponseService.getSuccessResult();
    }
}
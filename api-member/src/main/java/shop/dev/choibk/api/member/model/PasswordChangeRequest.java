package shop.dev.choibk.api.member.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PasswordChangeRequest {
    @ApiModelProperty(notes = "현재 비밀번호", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String currentPassword;

    @ApiModelProperty(notes = "변경할 비밀번호", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String changePassword;

    @ApiModelProperty(notes = "변경할 비밀번호 확인", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String changePasswordRe;
}
package shop.dev.choibk.api.member.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import shop.dev.choibk.api.member.entity.Member;
import shop.dev.choibk.api.member.model.MemberCreateRequest;
import shop.dev.choibk.api.member.model.PasswordChangeRequest;
import shop.dev.choibk.api.member.repository.MemberRepository;
import shop.dev.choibk.common.enums.MemberGroup;
import shop.dev.choibk.common.exception.CMissingDataException;
import shop.dev.choibk.common.exception.CWrongPhoneNumberException;
import shop.dev.choibk.common.function.CommonCheck;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;

    public void setFirstMember() {
        String username = "superadmin";
        String password = "abcd1234";
        boolean isSuperAdmin = isNewUsername(username);

        if (isSuperAdmin) {
            MemberCreateRequest createRequest = new MemberCreateRequest();
            createRequest.setUsername(username);
            createRequest.setPassword(password);
            createRequest.setPasswordRe(password);
            createRequest.setName("최고관리자");

            setMember(MemberGroup.ROLE_ADMIN, createRequest);
        }
    }

    public void setMember(MemberGroup memberGroup, MemberCreateRequest createRequest) {
        if (!CommonCheck.checkUsername(createRequest.getUsername()))
            throw new CWrongPhoneNumberException(); // 유효한 아이디 형식이 아닙니다로 던지기
        if (!createRequest.getPassword().equals(createRequest.getPasswordRe()))
            throw new CWrongPhoneNumberException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isNewUsername(createRequest.getUsername())) throw new CWrongPhoneNumberException(); // 중복된 아이디가 존재합니다 던지기

        createRequest.setPassword(passwordEncoder.encode(createRequest.getPassword()));

        Member member = new Member.MemberBuilder(memberGroup, createRequest).build();
        memberRepository.save(member);

    }

    private boolean isNewUsername(String username) {
        long dupCount = memberRepository.countByUsername(username);
        return dupCount <= 0;
    }

    public void putPassword(long memberId, PasswordChangeRequest changeRequest, boolean isAdmin) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);

        if (!isAdmin && !passwordEncoder.matches(changeRequest.getChangePassword(), member.getPassword()))
            throw new CMissingDataException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!changeRequest.getChangePassword().equals(changeRequest.getChangePasswordRe()))
            throw new CWrongPhoneNumberException(); // 변경할 비밀번호가 일치하지 않습니다. 던지기

        String passwordResult = passwordEncoder.encode(changeRequest.getChangePassword());
        member.putPassword(passwordResult);
        memberRepository.save(member);
    }
}
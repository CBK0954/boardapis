package shop.dev.choibk.api.member.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shop.dev.choibk.common.exception.CAccessDeniedException;
import shop.dev.choibk.common.exception.CAuthenticationEntryPointException;
import shop.dev.choibk.common.response.model.CommonResult;

@RestController
@RequestMapping("/exception")
public class ExceptionController {
    @GetMapping("/access-denied")
    public CommonResult accessDeniedException() {
        throw new CAccessDeniedException();
    }

    @GetMapping("/entry-point")
    public CommonResult entryPointException() {
        throw new CAuthenticationEntryPointException();
    }

}